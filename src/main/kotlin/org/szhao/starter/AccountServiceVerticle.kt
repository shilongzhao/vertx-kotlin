package org.szhao.starter

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.szhao.util.*

class AccountServiceVerticle : CoroutineVerticle() {
  companion object {
    private val logger = LogManager.getLogger(AccountServiceVerticle::class.java)

    const val ADDRESS = "service.account"
    const val CREATE_ACCOUNT = "service.account.create"
    const val GET_ACCOUNTS = "service.accounts.get"
  }

  override suspend fun start() {
    vertx.eventBus.consumer<JsonObject>(ADDRESS) {
      launch { dispatch(it) }
    }
  }

  private suspend fun dispatch(message: Message<JsonObject>) {
    val action = message.headers["action"]
    val body = message.body
    when (action) {
      CREATE_ACCOUNT -> eitherHandler(message, this::createAccount)
      GET_ACCOUNTS -> eitherHandler(message, this::getAccounts)
      else -> message.fail("Unknown action $action for request $body")

    }
  }

  /**
   * handle exceptions in the top level
   */
  private suspend fun <T, R> eitherHandler(
    message: Message<T>,
    handle: suspend (T) -> R
  ) =
    when (val result = coroutineTry { handle(message.body) }) {
      is Left -> {
        logger.error("failed request ${message.body}, error ${result.cause.message}")
        message.fail(-1, result.cause.message)
      }
      is Right -> {
        logger.info("successful get result ${result.value}")
        message.reply(result.value)
      }
    }


  private suspend fun createAccount(account: JsonObject): JsonObject {
    val password = encrypt(account["password"])
    account.put("password", password)
    val options = action(AccountDatabaseVerticle.INSERT_ACCOUNT)
    val re = vertx.eventBus
      .request<JsonObject>(AccountDatabaseVerticle.ADDRESS, account, options)
      .await()
      .body
    logger.info("saved account $account result $re")
    return re
  }

  private suspend fun getAccounts(filter: JsonObject): JsonArray =
    vertx.eventBus
      .request<JsonArray>(
        AccountDatabaseVerticle.ADDRESS,
        filter,
        action(AccountDatabaseVerticle.GET_ACCOUNTS)
      )
      .await()
      .body


  // TODO
  private fun encrypt(input: String): String {
    return "SALT$$input"
  }

  private fun action(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))
}
