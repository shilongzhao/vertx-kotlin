package org.szhao.starter

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.jdbcclient.JDBCPool
import io.vertx.kotlin.core.json.array
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.jdbcclient.jdbcConnectOptionsOf
import io.vertx.kotlin.sqlclient.poolOptionsOf
import io.vertx.sqlclient.Tuple
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.szhao.util.*
import java.util.*
import java.util.concurrent.TimeUnit


class AccountDatabaseVerticle : CoroutineVerticle() {
  companion object {
    private val logger = LogManager.getLogger(AccountDatabaseVerticle::class.java)

    const val ADDRESS = "database.account.address"
    const val INSERT_ACCOUNT = "database.account.insert"
    const val GET_ACCOUNTS = "database.accounts.get"
  }

  /**
   * Describe what a query is, this is actually similar to
   *
   * private fun query(statement: String, params: JsonArray, pool: JDBCPool) {
   *
   * }
   */
  private val getAccountsQuery: suspend JDBCPool.(JsonObject) -> JsonArray = {/* JDBCPool*/_ ->
    val ex = query("SELECT * FROM account LIMIT 100").execute().await()
    val values = json { array (ex.toList().map { it.toJson()}) }
    logger.debug("got accounts ${values.encode()}")
    values
  }

  // separates description from implementation
  // A query is something that needs a pool/connection to run
  // A runnable is something that needs an execution context/ thread to run (similar to the functional def of Actor)
  // like currying
  val insertAccountsQuery: suspend JDBCPool.(List<Tuple>) -> JsonArray = { /* JDBCPool */ acc ->
    logger.debug("inserting accounts ${acc[0]}")
    val rs = preparedQuery("INSERT INTO account(id, email, password) values (?, ?, ?)")
      .executeBatch(acc)
      .await()
    val values =  json { array(rs.toList().map { row -> row.getValue(0) }) }
    logger.debug("inserting account result $values")
    values
  }

  override suspend fun start() {
    logger.debug("deploying maria client verticle")

    val jdbcConf: JsonObject = config["jdbc"]

    val poolOptions = poolOptionsOf(
      idleTimeout = 100,
      idleTimeoutUnit = TimeUnit.MILLISECONDS,
      maxSize = 10,
      maxWaitQueueSize = 100
    ) // TODO: read from conf

    val connectOptions = jdbcConnectOptionsOf(
      user = jdbcConf["user"],
      password = jdbcConf["password"],
      jdbcUrl = jdbcConf["url"]
    )
    logger.debug("connect options ${connectOptions.toJson().encode()}")

    val pool = JDBCPool.pool(vertx, connectOptions, poolOptions)

    vertx.eventBus.consumer<JsonObject>(ADDRESS).handler {
      launch {
        dispatch(pool, it)
      }
    }

    logger.debug("maria client verticle started with $pool")
  }

  private suspend fun dispatch(pool: JDBCPool, message: Message<JsonObject>) {
    when (val action = message.headers["action"]) {
      INSERT_ACCOUNT ->
        eitherHandler(pool, message, this::insertAccount)
      GET_ACCOUNTS ->
        eitherHandler(pool, message, this::getAccounts)
      else ->
        message.fail("Unknown command $action")
    }

  }

  private suspend fun <T, R> eitherHandler(
    pool: JDBCPool,
    message: Message<T>,
    databaseHandler: suspend (JDBCPool, T) -> R
  ) {
    when (val result = coroutineTry { databaseHandler(pool, message.body) }) {
      is Left -> message.fail("failed reason ${result.cause.message}")
      is Right -> message.reply(result.value)
    }
  }

  private suspend fun insertAccount(pool: JDBCPool, account: JsonObject): JsonObject {
    logger.debug("inserting account $account")
    val id = UUID.randomUUID().toString()
    val tuple = Tuple.of(id, account["email"], account["password"])
    try {
      val r = insertAccountsQuery(pool, listOf(tuple))
      logger.info("inserted account $account result $r")
      return account.copy().put("password", "")
    } catch (t: Throwable) {
      logger.error("error $t")
      throw t
    }
  }

  private suspend fun getAccounts(pool: JDBCPool, filter: JsonObject): JsonArray {
    val array = getAccountsQuery.invoke(pool, filter)
    logger.info("get accounts $array for filter $filter")
    return array
  }


}
