package org.szhao.starter

import io.vertx.config.ConfigRetriever
import io.vertx.core.CompositeFuture
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.config.configRetrieverOptionsOf
import io.vertx.kotlin.config.configStoreOptionsOf
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.apache.logging.log4j.LogManager

class MainVerticle : CoroutineVerticle() {
  companion object {
    private val logger = LogManager.getLogger(MainVerticle::class.java)
  }

  /**
   * configuration loader, should be able to read configuration from git, database, ...
   * here we just use the file configuration
   */
  override suspend fun start() {
    val envs = System.getenv().entries
      .filter { it.key.startsWith("ZHAO") }
      .joinToString(", ") { "${it.key} = ${it.value}" }

    logger.debug("environment variables: $envs")

    val conf = if (!config.isEmpty) config else readConf()
    logger.debug("application configuration: $conf")

    val verticles = listOf(
      AccountServiceVerticle::class.java,
      HttpServerVerticle::class.java,
      AccountDatabaseVerticle::class.java)

    val futures = verticles.map {
      vertx.deployVerticle(it, deploymentOptionsOf(config = conf))
    }


    /**
     * We need a join point here, since in the unit test
     * we need all the verticles to be ready before a test starts.
     *
     * of course, there could be alternative implementation,
     * but pay attention to the pitfalls:
     *
     * In this way, the verticles are deployed one after another:
     *
     * verticles.forEach {
     *   vertx.deployVerticle(it, deploymentOptionsOf(config = conf)).await()
     * }
     *
     *
     * In this way, the verticles are deployed concurrently but no join point:
     *
     * verticles.forEach {
     *   launch {
     *     vertx.deployVerticle(it, deploymentOptionsOf(config = conf)).await()
     *   }
     * }
     *
     */
    CompositeFuture.all(futures).await()
    logger.info("main verticle started")
  }

  // TODO: change config to some lazy val?
  private suspend fun readConf(): JsonObject {
    val filename = env["ZHAO_CONF_FILE"]
    logger.debug("configuration using environment variable ZHAO_CONF_FILE=$filename")
    val localJsonStore =
      configStoreOptionsOf(jsonObjectOf("path" to filename), type = "file", format = "hocon", optional = false)
    val retrieverOptions = configRetrieverOptionsOf(stores = listOf(localJsonStore))
    val configRetriever = ConfigRetriever.create(vertx, retrieverOptions)
    return configRetriever.config.await()
  }

  private val env: Map<String, String>
    get() = System.getenv()

}
