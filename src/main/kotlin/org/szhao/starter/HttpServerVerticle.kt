package org.szhao.starter

import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CSRFHandler
import io.vertx.ext.web.handler.SessionHandler

import io.vertx.ext.web.openapi.Operation
import io.vertx.ext.web.openapi.RouterBuilder
import io.vertx.ext.web.sstore.LocalSessionStore
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.http.httpServerOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.core.net.pemKeyCertOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.launch
import org.apache.logging.log4j.LogManager
import org.szhao.util.*

// on 8080 port, serves both WebSocket ws:// and HTTP http://
class HttpServerVerticle : CoroutineVerticle() {
  companion object {
    private val logger = LogManager.getLogger(HttpServerVerticle::class.java.name)
    private const val KB = 1024L
    private const val MB = 1024 * KB

  }

  override suspend fun start() {
    val builder = RouterBuilder.create(vertx, "src/main/resources/api_v1.yaml").await()

    builder.operation("getAccounts").coroutineHandler { getAccounts(it) }
    builder.operation("createAccount").coroutineHandler { createAccount(it) }

    val global = Router.router(vertx)
    val router = builder.createRouter()
    global.mountSubRouter("/v1", router)
    val bodyHandler = BodyHandler.create().setBodyLimit(10 * MB)
    val csrfHandler = CSRFHandler.create(vertx, "{!!secret_csrf@replace!!}")
    val cookieHandler = SessionHandler.create(LocalSessionStore.create(vertx))
      .setCookieHttpOnlyFlag(true).setCookieSecureFlag(true)

    val httpConf: JsonObject = config["http_server"]
    val keyPath: String = httpConf["ssl_key_file"]
    val certPath: String = httpConf["ssl_cert_file"]

    val port: Int = httpConf["port"]
    val host: String = httpConf["host"]
    val key = vertx.fileSystem().readFile(keyPath).await()
    val cert = vertx.fileSystem().readFile(certPath).await()

    val pemKeyCertOptions = pemKeyCertOptionsOf(keyValue = key, certValue = cert)
    val httpServerOptions = httpServerOptionsOf(ssl = true, pemKeyCertOptions = pemKeyCertOptions)


    global.route().handler(bodyHandler).handler(csrfHandler).handler(cookieHandler)
    // NOTE: The hostname is not localhost here (as we usually see). It actually depends on the SSL certificates
    vertx.createHttpServer(httpServerOptions).requestHandler(global).listen(8080, "www.szhao.org")

    logger.info("deployed http server verticle at localhost port 8080")
  }

  private suspend fun <T> eitherHandler(
    context: RoutingContext,
    request: JsonObject,
    handler: suspend (JsonObject) -> T
  ) {
    when (val result = coroutineTry { handler(request) }) {
      is Right -> context.response.end(result.value.toString())
      is Left -> context.response.end(jsonObjectOf("code" to 400, "message" to result.cause.message))
    }
  }


  private suspend fun createAccount(context: RoutingContext) =
    eitherHandler(context, context.body.toJsonObject(), this::createAccountJson)

  private suspend fun createAccountJson(jsonObject: JsonObject): JsonObject {
    val deliveryOptions = action(AccountServiceVerticle.CREATE_ACCOUNT)
    return vertx
      .eventBus
      .request<JsonObject>(AccountServiceVerticle.ADDRESS, jsonObject, deliveryOptions)
      .await()
      .body
  }


  private suspend fun getAccounts(ctx: RoutingContext) {
    eitherHandler(ctx, JsonObject(), this::getAccountsJson)
  }

  private suspend fun getAccountsJson(jsonObject: JsonObject): JsonArray =
    vertx.eventBus
      .request<JsonArray>(
        AccountServiceVerticle.ADDRESS, jsonObject,
        action(AccountServiceVerticle.GET_ACCOUNTS)
      )
      .await().body


  private fun action(action: String) = deliveryOptionsOf(headers = mapOf("action" to action))

  private fun Operation.coroutineHandler(fn: suspend (RoutingContext) -> Unit) =
    handler { launch { fn(it) } }

  private fun HttpServerResponse.end(json: JsonObject) = end(json.encode())

  private val RoutingContext.response: HttpServerResponse
    get() = response()
}
