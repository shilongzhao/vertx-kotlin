package org.szhao.util

import io.vertx.core.MultiMap
import io.vertx.core.eventbus.Message

fun <T> Message<T>.fail(string: String) = fail(-1, string)

val <T> Message<T>.body: T
  get() = body()

val <T> Message<T>.headers: MultiMap
  get() = headers()
