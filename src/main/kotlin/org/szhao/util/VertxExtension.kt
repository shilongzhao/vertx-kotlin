package org.szhao.util

import io.vertx.core.Vertx
import io.vertx.core.eventbus.EventBus

val Vertx.eventBus: EventBus
  get() = eventBus()
