package org.szhao.util


sealed class Either<out T, out R>

data class Left<out T>(val cause: T) : Either<T, Nothing>()

data class Right<out R>(val value: R) : Either<Nothing, R>()

suspend fun<T> coroutineTry(f: suspend () -> T): Either<Throwable, T> = try { Right(f()) } catch (t: Throwable) { Left(t) }


