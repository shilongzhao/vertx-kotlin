package org.szhao.util

import io.vertx.core.json.JsonObject

@Suppress("UNCHECKED_CAST") operator fun <T> JsonObject.set(key: String, value: T): JsonObject = put(key, value)
