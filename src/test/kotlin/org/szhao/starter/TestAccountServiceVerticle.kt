package org.szhao.starter

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.eventbus.deliveryOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.szhao.util.*

@ExtendWith(VertxExtension::class)
class TestAccountServiceVerticle : CoroutineVerticle() {
  companion object {
    @BeforeAll
    @JvmStatic
    fun startSystem(vertx: Vertx) = runBlocking {
      val conf = readTestConfiguration(vertx)
      startVertxSystem(vertx, conf)
    }
  }
  @BeforeEach
  fun clearTables(vertx: Vertx) = runBlocking(vertx.dispatcher()) {
    val conf = readTestConfiguration(vertx)
    initTables(vertx, conf.getJsonObject("jdbc"))
  }


  @Test
  fun createAndGetAccountsTest(vertx: Vertx, testContext: VertxTestContext) = runBlocking(vertx.dispatcher()) {
    val created = createAccount(vertx, "admin@szhao.org", "enigma")
    Assertions.assertFalse(created.isEmpty)
    val get = getAllAccounts(vertx)
    Assertions.assertEquals(1, get.size())
    testContext.completeNow()
  }

  private suspend fun createAccount(vertx: Vertx, email: String, password: String) =
    vertx.eventBus
      .request<JsonObject>(
        AccountServiceVerticle.ADDRESS,
        jsonObjectOf("email" to email, "password" to password),
        deliveryOptionsOf(headers = mapOf("action" to AccountServiceVerticle.CREATE_ACCOUNT))
      )
      .await()
      .body

  private suspend fun getAllAccounts(vertx: Vertx) =
    vertx.eventBus
      .request<JsonArray>(
        AccountServiceVerticle.ADDRESS, JsonObject(),
        deliveryOptionsOf(headers = mapOf("action" to AccountServiceVerticle.GET_ACCOUNTS))
      )
      .await().body
}
