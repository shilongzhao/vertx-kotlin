package org.szhao.starter

import io.vertx.config.ConfigRetriever
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.jdbcclient.JDBCPool
import io.vertx.kotlin.config.configRetrieverOptionsOf
import io.vertx.kotlin.config.configStoreOptionsOf
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await
import io.vertx.kotlin.jdbcclient.jdbcConnectOptionsOf
import io.vertx.kotlin.sqlclient.poolOptionsOf
import java.util.concurrent.TimeUnit

suspend fun readTestConfiguration(vertx: Vertx): JsonObject {
  val filename = "config.unit.conf"
  val localJsonStore = configStoreOptionsOf(jsonObjectOf("path" to filename), type = "file", format = "hocon", optional = false)
  val retrieverOptions = configRetrieverOptionsOf(stores = listOf(localJsonStore))
  val configRetriever = ConfigRetriever.create(vertx, retrieverOptions)
  return configRetriever.config.await()
}

suspend fun initTables(vertx: Vertx, jdbcConf: JsonObject) {

  val poolOptions = poolOptionsOf(
    idleTimeout = 100,
    idleTimeoutUnit = TimeUnit.MILLISECONDS,
    maxSize = 2,
    maxWaitQueueSize = 20
  )

  val connectOptions = jdbcConnectOptionsOf(
    user = jdbcConf.getString("user"),
    password = jdbcConf.getString("password"),
    jdbcUrl = jdbcConf.getString("url")
  )
  val pool = JDBCPool.pool(vertx, connectOptions, poolOptions)
  // drop and create tables
  pool.query("""
    DROP TABLE IF EXISTS `account`;
    CREATE TABLE `account`(
        `id` CHAR(36) NOT NULL PRIMARY KEY,
        `email` char(255) NOT NULL UNIQUE,
        `password` VARCHAR(255) NOT NULL
    );
  """.trimIndent()).execute().await()

  // complete
}


suspend fun startVertxSystem(vertx: Vertx, conf: JsonObject) {
  vertx.deployVerticle(MainVerticle(), deploymentOptionsOf(config = conf)).await()
}
