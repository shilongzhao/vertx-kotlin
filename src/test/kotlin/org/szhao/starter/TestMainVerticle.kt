package org.szhao.starter

import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
class TestMainVerticle {
  companion object {
    @BeforeAll
    @JvmStatic
    fun startSystem(vertx: Vertx, testContext: VertxTestContext) = runBlocking {
      val conf = readTestConfiguration(vertx)
      startVertxSystem(vertx, conf)
      testContext.completeNow()
    }
  }
  @BeforeEach
  fun clearTables(vertx: Vertx, testContext: VertxTestContext) = runBlocking(vertx.dispatcher()) {
    val conf = readTestConfiguration(vertx)
    initTables(vertx, conf.getJsonObject("jdbc"))
    testContext.completeNow()
  }


  @Test
  fun verticleDeployed(testContext: VertxTestContext) {
    testContext.completeNow()
  }
}
