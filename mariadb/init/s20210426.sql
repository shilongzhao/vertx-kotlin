CREATE DATABASE IF NOT EXISTS `starter_db`;

USE starter_db;

CREATE TABLE IF NOT EXISTS `account`(
    `id` CHAR(36) NOT NULL PRIMARY KEY,
    `email` char(255) NOT NULL UNIQUE,
    `password` VARCHAR(255) NOT NULL
);
