import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.tasks.testing.logging.TestLogEvent.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
  kotlin("jvm") version "1.4.21"
  application
  id("com.github.johnrengelman.shadow") version "6.1.0"
}

group = "org.szhao"
version = "1.0.0-SNAPSHOT"

repositories {
  mavenCentral()
  jcenter()
}

val vertxVersion = "4.0.3"
val junitJupiterVersion = "5.7.0"
val logbackVersion = "1.2.3"
val h2DatabaseVersion = "1.4.200"
val postgresqlVersion = "42.2.18"

val mainVerticleName = "org.szhao.starter.MainVerticle"
val launcherClassName = "io.vertx.core.Launcher"

val watchForChange = "src/**/*"
val doOnChange = "${projectDir}/gradlew classes"

application {
  mainClass.set(launcherClassName)
}
project.setProperty("mainClassName", launcherClassName)

dependencies {
  implementation(platform("io.vertx:vertx-stack-depchain:$vertxVersion"))
  implementation("io.vertx:vertx-web")
  implementation("io.vertx:vertx-web-openapi")

  implementation("io.vertx:vertx-config")
  implementation("io.vertx:vertx-config-hocon")
  implementation("io.vertx:vertx-grpc")
  implementation("io.vertx:vertx-lang-kotlin-coroutines")
  implementation("io.vertx:vertx-json-schema")
  implementation("io.vertx:vertx-shell")
  implementation("io.vertx:vertx-lang-kotlin")

  // cluster
  implementation("io.vertx:vertx-hazelcast")

  // data store
  implementation("io.vertx:vertx-redis-client")
  implementation("io.vertx:vertx-jdbc-client")
  implementation("org.mariadb.jdbc:mariadb-java-client:2.7.2")
  implementation("io.agroal:agroal-api:1.10")
  implementation("io.agroal:agroal-pool:1.10")
  // messages
  implementation("io.vertx:vertx-kafka-client")

  // metrics

  implementation(kotlin("stdlib-jdk8"))

  // logging
  implementation("org.apache.logging.log4j:log4j-api:2.14.1")
  implementation("org.apache.logging.log4j:log4j-core:2.14.1")
  // test
  testImplementation("com.h2database:h2:$h2DatabaseVersion")

  testImplementation("io.vertx:vertx-junit5")
  testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
  testImplementation("io.rest-assured:rest-assured:3.3.0")

}


// repositories, dependencies, etc...
val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"

tasks.withType<Test> {
  useJUnitPlatform()
  testLogging {
    events = setOf(PASSED, SKIPPED, FAILED)
  }
}

tasks.withType<JavaExec> {
  args = listOf(
    "run", mainVerticleName,
    "--redeploy=$watchForChange",
    "--launcher-class=$launcherClassName",
    "--on-redeploy=$doOnChange"
  )
}

tasks.withType<ShadowJar> {
  archiveClassifier.set("fat")
  manifest {
    attributes(mapOf("Main-Verticle" to mainVerticleName))
  }
  mergeServiceFiles()
}

